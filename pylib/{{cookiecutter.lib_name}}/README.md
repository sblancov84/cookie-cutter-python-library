# {{cookiecutter.lib_name}}

{{cookiecutter.description}}

# Contributing as developer

For more information about development just consult the
[developer guide](./docs/developer.md)

# Used Technologies

For more information about technologies just consult
[technologies document](./docs/technologies.md)
