# Developer guide

This guide consider that the local machine (where everything always works) is
Debian. So for other operating systems must be adapted.

## Prepare development environment

In order to save time, some of tools are helpful.

Check [Used technologies](./technologies.md) for more information about all of
them.

### Make

Make is an old tool, but a great one. It helps to remember all frequently
commands you type again and again.

### Pyenv

It helps to fix the python version for the project environment and to create the
virtual environment to develop.

There is a registered marker called "current" in the pytest configuration that
it could be useful to do TDD.

### Autoenv

It executes a script when you change to the target directory and execute another
when you leave it.

### Docker

It create an isolated space to execute as if the operating system is only for
target application/service.

## Lifecycle

This section explains all of steps of the lifecycle of this library.

### Coding and testing

Well, they are two activities that should be done at the same time. Unit testing
and coding are inseparable process. Test Driven Development (TDD) encourage
this.

### Analysis

Standards help to make less mistakes, if the code respect some of coding
standards created by the community, the code will be better, more readable and
maintainable. Make less mistakes helps to keep low costs, so linters are
valuable tools, specially when its very frequent to do the kind of mistakes that
this type of tools prevents. A linter integrated into IDE is a good idea.

### Testing

Thought testing is done at the same time we are coding, we can touch something
that brake other things, so we need to execute the whole test suite to prevent
regressions.

### Package

Code has to be deployed in an easy manner and in many environments, so we need
to build a package which we can provide to our clients. The way to do this is to
create a bundle with everything that the code needs to run in any environment.

### Publish

Store the bunlde of library into a binary repository to distribute it to the
clients. Some place where a client can get any version of this library. In this
case it could be PyPI or any other repository (public or private).
