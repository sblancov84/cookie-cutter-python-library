from setuptools import setup, find_packages


def get_install_requires():
    with open("requirements/run.txt") as descriptor:
        return list([
            dependency.strip("\n") for dependency in descriptor.readlines()])


def get_long_description():
    with open("README.md", "r") as descriptor:
        long_description = descriptor.read()
    return long_description


description = '{{cookiecutter.description}}'
_license = 'MIT'


if __name__ == "__main__":
    setup(
        name='{{cookiecutter.lib_name}}',
        version='0.1.0',
        description=description,
        project_urls={
            'Source': '{{cookiecutter.url}}'
        },
        author='{{cookiecutter.author}}',
        author_email='{{cookiecutter.email}}',
        license=_license,
        packages=find_packages(exclude=['docs']),
        long_description=get_long_description(),
        long_description_content_type='text/markdown',
        python_requires='>={{cookiecutter.python_version}}',
        install_requires=get_install_requires(),
        classifiers=[
            'Intended Audience :: Developers',
            f'License :: OSI Approved :: {_license} License',
            'Operating System :: OS Independent',
            'Programming Language :: Python',
            'Programming Language :: Python :: {{cookiecutter.python_version}}',
        ],
    )
